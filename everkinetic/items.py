# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class EverkineticItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    pass

class ExerciseItem(scrapy.Item):
    name = scrapy.Field()
    steps = scrapy.Field()
    exercise_type = scrapy.Field()
    primary_muscles = scrapy.Field() 
    secondary_muscles = scrapy.Field()
    equipment = scrapy.Field() 
    image_urls = scrapy.Field()
    file_paths = scrapy.Field()