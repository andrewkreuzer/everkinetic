import scrapy
from ..items import ExerciseItem
import logging


class exerciseSpider(scrapy.Spider):
    name = "exercise_scraper"

    def start_requests(self):
        base_url = 'http://db.everkinetic.com'
        urls = [base_url]

        for page in range(2,26):
            urls.append(f'{base_url}/page/{page}')

        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        for a in response.css('article.exercise h3 a'):
            yield response.follow(a, callback=self.parse_exercise_info)

    def parse_exercise_info(self, response):
        entry_main = response.css('div.entry-main')
        content = entry_main.css('div.exercise-entry-content')
        exercise_taxonomy_list_elements = content.css('ul.exercise-taxonomies li')

        name = entry_main.css('h1.entry-title a::text').extract()[0]
        steps = content.css('ol li::text').extract()

        for element in exercise_taxonomy_list_elements:
            text = element.css('strong::text').extract_first()
            if text == 'Type: ':
                exercise_type = element.css('a::text').extract()
            #Deal with glutes being spelt gluts
            #and look for others
            if text == 'Primary: ':
                primary_muscles = element.css('a::text').extract()
            if text == 'Secondary: ':
                secondary_muscles = element.css('a::text').extract()
            if text == 'Equipment: ':
                equipment = element.css('a::text').extract()
            
        image_list = entry_main.css('ul.exercise-images li')
        image_urls = image_list.css('img::attr(src)').extract()

        yield ExerciseItem(
            name = name if 'name' in locals() else None,
            steps = steps if 'steps' in locals() else None,
            exercise_type = exercise_type if 'exercise_type' in locals() else None,
            primary_muscles = primary_muscles if 'primary_muscles' in locals() else None,
            secondary_muscles = secondary_muscles if 'secondary_muscles' in locals() else None,
            equipment = equipment if 'equipment' in locals() else None,
            image_urls = image_urls
        )
